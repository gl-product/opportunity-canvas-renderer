This repository allows you to generate an opportunity canvas by writing
Markdown, rather than filling out a Google Sheet or Mural template.

# Instructions

1. Fork this repository.
    1. Your fork should be set to Private visibility if you want to write
    sensitive info in the canvas.
1. Update `index.markdown` with your Opportunity Canvas content.
1. Build the static site.
    1. This should happen automatically with the GitLab CI pipeline.
1. Publish, review, and discuss the generated canvas!
