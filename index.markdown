---
layout: home

problem: {
    certainty: "medium",
    description: "Lorem ipsum",
    personas: "Lorem",
    pain: Lorem "ipsum",
    pain_level: "Lorem",
    workarounds: "Lorem",
}

business-case: {
    certainty: "medium",
    description: "Lorem ipsum",
    reach: "Lorem ipsum",
    impact: "Lorem ipsum",
    confidence: "Lorem ipsum",
    confidence-level: "medium",
}

solution: {
    certainty: "medium",
    description: "medium",
    viability: "Lorem ipsum",
    iteration: "Lorem ipsum",
    differentiation: "Lorem ipsum",
}

launch: {
    certainty: "medium",
    description: "lorem",
    measures: "lorem",
    headline: "lorem",
    first-paragraph: "lorem",
    customer-quote: "lorem",
    go-to-market: "lorem",
}

learnings: [
    {
        assumption: "my assumption",
        thought-before: "what I thought",
        learned: "what I learned",
        think-now: "what I think now",
    },
    {
        assumption: "another assumption",
        thought-before: "what I thought",
        learned: "what I learned",
        think-now: "what I think now",
    },
]

learning-goals: [
    {
        hypothesis: "hypothesis to test",
        test-method: "way to quickly test",
    },
    {
        hypothesis: "another hypothesis to test",
        test-method: "another way to quickly test",
    }
]

---

Error! You shouldn't see this.

## Troubleshooting

1. Make sure all your entires have quotation marks at the start and end of them.
1. Make sure if you use any quotation marks in your lines that they are escaped.
1. Make sure that each line ends with a comma.
